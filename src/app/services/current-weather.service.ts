import { Injectable, isDevMode } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Subject, Observable, from } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { Coords } from "src/structures/coords.structure";
import { weather } from "src/structures/weather.structure";

@Injectable({
  providedIn: "root",
})
export class CurrentWeatherService {
  public weatherSubject: Subject<any> = new Subject<any>();
  public weather$: Observable<any>;
  public endpoint: string = "https://api.openweathermap.org/data/2.5/weather";
  constructor(private http: HttpClient) {
    this.weather$ = this.weatherSubject.asObservable().pipe(
      map((data: any) => {
        let mainWeather = data.weather[0];
        let weather: weather = {
          name: data.name,
          cod: data.cod,
          temp: data.main.temp,
          ...mainWeather,
        };
        return weather;
      })
    );
    this.get({
      lat: 35,
      lon: 139,
    });
  }

  get(coords: Coords) {
    let args: string = `?lat=${coords.lat}&lon=${coords.lon}&appid=${environment.key}&units=metric`;
    let url = this.endpoint + args;
    if (isDevMode()) {
      url = "assets/wheather.json";
    }
    this.http.get(url).subscribe(this.weatherSubject);
  }
  // Video - obtener el clima actual min 5:41
  //Subject - Es un observable y observer al tiempo
  //Es necesario un subject cuando necesitamos un intermediario o proxy

  //Observable - son objetos que retorna el metodo get de http a los que nos debemos de suscribir y nos entregan info
  //Observer - Cumplen con una interfaz observer, que la libreria define con metodos como next o complete que se utilizan suscriptores de los observables
  //Observer.next genera notificaciones de que recibieron informacion del observable
}
